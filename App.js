/**
 * @author Fahmida Chowdhury
 * @version 1.0
 * NOTE: image subject to change
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';


export default class App extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
       <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
      <Image
        style={{width: 200, height: 40, top: '45%'}}
        source={require('./asset/logo.png')}
      />
      <Text style={styles.h2}>
      Connect with people and do what you love.
      </Text>
      </View>
       <View style={{flex:1}}>
      <TouchableOpacity onPress={() => console.log('go to page to select cravings')} style={styles.sButton}>
      <Text style={styles.sButtonText}> Sign Up With Facebook </Text>
      </TouchableOpacity>
      </View>
       <View style={{flex:1}}>
      <Image
      resizeMode={'contain'}
      style={{flex: .88, width: 500, top: '0%'}}
      source={require('./asset/city.png')}
      />
       </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#edf0ed',
  },
  bgContainer: {
    flex: 1,
    width: 500,
  },
  h2: {
    fontSize: 24,
    color: '#b0bec5',
    top: '45%',
    width: 300,
    alignContent: 'center',
    textAlign: 'center',
  },
  sButton: {
    backgroundColor: '#3b5998',
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 10,
    top: '40%',
  },
  sButtonText: {
    color: '#ffffff',
    fontSize: 18
  }

});
